'''
ログレベルを指定して出力する
debug　　：低
info 　　：↓
warning　：↓
error  　：↓
critical ：高
'''
import logging
#test.logというファイルに出力する
#WARNINGレベル以上がログに出力される
logging(filename='test.log', level=logging.DEBUG)
logging.debug('debug level message')
logging.info('info level message')
logging.warning('warning level message')