def kugiri():
    print('-' * 20)

'''
loggingモジュールログを出力するための
モジュールです
ログには
debug　　：テストで情報を出力する
info 　　：動作状況の報告
warning　：エラーが起きる予兆としての警告
error  　：エラー情報
critical ：システム故障に直結するエラー
'''
import logging
#warning以上が標準エラー出力される
logging('debug level message')
logging('info level message')
logging('warning level message')
logging('error level message')
logging('critical level message')