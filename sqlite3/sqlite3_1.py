'''
sqlite3を利用します
簡単に使えるRDBMSです
・Windows
　dllをダウンロードしてインストールする必要あり
　C:\windwos\system32にdllを入れる必要あり
・Mac
　最初から既にインストールされているようだ・・・
　(未確認)
'''
import sqlite3

#データベースのファイル名を指定する
db = 'sample.db'

#接続オブジェクトを生成する
con = sqlite3.connect(db)

#カーソルオブジェクトを生成する
csr = con.cursor()

#テーブルを生成するSQL
#テーブルとは、データを保存する表
'''
SQLはDBを操作する言語です
3種類の命令から成り立ちます
DDL：定義を行う
DML：データを操作する
DCL：データを制御する
create tableはDDL
'''
sql = 'create table tbl_student(student_number, name, email);'
#executeでsqlを実行する
csr.execute(sql)

#commitでDBに保存する
con.commit()

#closeで接続を閉じる
con.close()