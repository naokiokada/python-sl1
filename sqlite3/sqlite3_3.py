import sqlite3

db = 'sample.db'
con = sqlite3.connect(db)
cur = con.cursor()

#テーブルからデータの削除を行う
#delete文実行
#where は操作するデータの条件を指定している
sql = "delete from tbl_student where student_number = '0L02002';"
cur.execute(sql)

#この時点のDB内容を確認する
sql = "select * from tbl_student;"
cur.execute(sql)
print(cur.fetchall())
print('-' * 20)
#レコードを更新します
#update文
sql = "update tbl_student set name = 'Taro test' where\
 student_number = '0L02001';"
cur.execute(sql)

con.commit()

#再度中身確認
sql = "select * from tbl_student;"
cur.execute(sql)
print(cur.fetchall())
print('-' * 20)

con.close()