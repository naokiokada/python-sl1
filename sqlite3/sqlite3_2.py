'''
sqlite3のDBのデータ操作
DML
を実行します
DMLは、4種類の操作があります
CRUD
C:create :insert文・・・登録
R:read   :select文・・・参照
U:update :update文・・・更新
D:delete :delete文・・・削除
'''
import sqlite3

db  = 'sample.db'
con = sqlite3.connect(db)
cur = con.cursor()

#insert文を2回実行する
sql = "insert into tbl_student(student_number, name, email)\
 values ('0L02001','テスト太郎','test@test.com');"
cur.execute(sql)
sql = "insert into tbl_student(student_number, name, email)\
 values ('0L02002','テスト花子','test2@test2.com');"
cur.execute(sql)

#この時点の内容を表示する
#select文を実行する
sql = "select * from tbl_student;"
cur.execute(sql)
print(cur.fetchall()) #select文はDBから参照内容をもらう
print('-' * 20)

con.commit()
con.close()