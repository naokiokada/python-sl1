'''
DBからテーブルを削除します
この操作は、DDLです
(create tableと同じ)
'''
import sqlite3

#データベースのファイル名を指定する
db = 'sample.db'

#接続オブジェクトを生成する
con = sqlite3.connect(db)

#カーソルオブジェクトを生成する
csr = con.cursor()

#テーブルを削除するsql
sql = 'drop table tbl_student;'
#executeでsqlを実行する
csr.execute(sql)

#commitでDBに保存する
con.commit()

#closeで接続を閉じる
con.close()