import re

def kugiri():
    print('-' * 20)

#こんなリストがあるとします
super_express = ['のぞみ3', 'のぞみ64', 'のぞみ150', 'ひかり440',
'ひかり538', 'こだま730']

result = []
for v in super_express:
    #\Dは数字以外1つという意味
    #+は繰り返しという意味
    #()はグループ分け
    g = re.search(r'(\D+)(\d+)', v)
    #数字1文字はこんな書き方もできる
    #^は指定以外
    #[0-9]は0～9でつまり数字
    #g = re.search(r'([^0-9]+)([0-9]+)', v)
    result.append((g.group(1), int(g.group(2))))

print(result)