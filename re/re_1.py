'''
柔軟な文字列処理を行うには
正規表現
を使います
正規表現はかなり奥が深い分野
ですが、その触りだけ紹介します
'''
def kugiri():
    print('-' * 20)

#こんなリストがあるとします
super_express = ['Nozomi3', 'Nozomi64', 'Nozomi150', 'Hikari440',
'Hikari538', 'Kodama730']

#例えばローマ字と数字をわけたい場合
import re

result = []
for v in super_express:
    #[a-zA-Z]はa～z、A～Zの1文字という意味
    #\dは数字1つという意味
    #+は繰り返しという意味
    #()はグループ分け
    g = (r'([a-zA-Z]+)(\d+)', v)
    result.append((g.group(1), int(g.group(2))))
    #Python3.6以上だとg[1],g[2]で大丈夫だそうだ

print(result)
kugiri()

for v in super_express:
    #[a-z]なので大文字は表示されない
    g = re.search(r'([a-z]+)(\d+)', v)
    print(g.group(1), g.group(2), sep=",")