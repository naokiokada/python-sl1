def kugiri():
    print('-' * 20)

'''
統計用のモジュールです
'''
#ここはデータ準備です
import random
random.seed(123)

#少数第一位で丸めた乱数を15個生成する
data = [round(random.random(), 1) for i in range(15)]
print(data)

#平均を求める
print(sum(data) / len(data))
kugiri()

#統計用モジュールを読み込む
import statistics
print(statistics.mean(data))
kugiri()
#最頻値を求める
#ただし、最頻値が求められない時はエラーになる
print(statistics.mode(data))

'''
ただし、あまり高機能な算術はできないので
統計や機械学習では
Numpyやpandasを使うことが多いです
'''