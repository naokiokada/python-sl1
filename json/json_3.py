def kugiri():
    print('-' * 20)

import json

#崩れたjsonを生成する
my_text = "{'a':1"

try:
    
    #このような例外をキャッチすると捕まえられる
except json.JSONDecodeError as e:
    print('Not JSON Format')
else:
    #tryが成功したらこれが実行される
    print('Json Format')