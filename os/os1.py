'''
osモジュールは、PythonがOSを操作する
為のモジュールです
とても便利で、linuxでもMacでもWindowsでも
それぞれのOSに合わせて、実行できます
'''
import os

def kugiri():
    print('-' * 20)

file_path = __file__
print(file_path)
kugiri()

#カレントディレクトリを読み込む
print((file_path))
kugiri()
#ディレクトリ判定
print((file_path))
kugiri()
#ファイル判定
print((file_path))
kugiri()

#ディレクトリ名を返す
#ディレクトリ名を指定すると親ディレクトリ
print((file_path))
kugiri()

#最後のファイル名
print((file_path))
kugiri()

#パス名、ファイル名の分割
dir_name, file_name = (file_path)
print(dir_name)
print(file_name)
kugiri()

#パス名、ファイル名をくっつける
print((dir_name, file_name))
kugiri()
print((file_path))
kugiri()